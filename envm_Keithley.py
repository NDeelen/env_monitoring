#!/usr/local/bin/python

import time, datetime, subprocess, os
from array import array
import imp

isTest = False

def getTimestamp():
	return str(time.time()).rstrip('\n')

def getVoltage(ch):
	if isTest:
		commandV = ["echo","300"]
	else:
		commandV = ["python", "keithleyControl.py","-p","/dev/ttyUSB"+ch,"--rv"]
	pipe = subprocess.Popen(commandV, stdout=subprocess.PIPE)
	text = pipe.communicate()
	return text[0].rstrip("\n")


def getCurrent(ch):
	if isTest:
		commandV = ["echo","0.1"]
	else:
		commandV = ["python", "keithleyControl.py","-p","/dev/ttyUSB"+ch,"--rc"]
	pipe = subprocess.Popen(commandV, stdout=subprocess.PIPE)
	text = pipe.communicate()
	return text[0].rstrip("\n")

if __name__=='__main__':

	if os.path.isfile("currents.csv"):
		newFile = False
	else:
		newFile = True
	currentFile = open("currents.csv",'a')
	if newFile:
		currentFile.write("timestamp_K0/D,voltage0/D,current0/D,voltage1/D,current1/D"+"\n")
	print "timestamp_K0 \t voltage0 \t current0 \t voltage1 \t current1"+"\n"	
	currentFile.close()
	while True:
		currentFile = open("currents.csv",'a')
		ts_v_0 = getTimestamp()
		
                v0 = getVoltage("0")[5:18]
		i0 = getCurrent("0")
		
		v1 = getVoltage("1")[5:18]
		i1 = getCurrent("1")

		currentFile.write(str(ts_v_0)+","+str(v0)+","+str(i0)+","+str(v1)+","+str(i1)+"\n")
		currentFile.close()
		print "!!" + str(ts_v_0)+"\t"+str(v0)+"\t"+str(i0)+"\t"+str(v1)+"\t"+str(i1)+"\n"
		    
		if isTest:
			break
		else:
			time.sleep(1)
	else : 	
		print "FALSE! ERROR!"
