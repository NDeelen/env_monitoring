# ToDo list:

- Writing a TTree instead of the currents.csv file.
- Check the separate env_monitoring scripts.
- Write readme for the temperature and the humidity sensors.
- ...

# Environmental monitoring for tests for DAQ for PhaseII Upgrade of CMS Tracker

To test the DAQ of the Ph2 CMSTK Upgrade environmental monitoring is something necessary. This can be monitoring the leakage current, temperature, humidity, and so fort and so on. Controlling sensors/power supplies that do these measurements is also required. 

This repo contains scripts to monitor and control all kinds of sensors.

## Getting started

For the Keithley control python 2.7 or higher is required. Installing python 2.7 on SL6 is not straight forward because it is not supported. Howover one can install multiple python versions on SL6 and use a sandbox for python the run the desired version.

### Installing python 2.7 on SL6

First install some prerequisites:

```
sudo yum groupinstall -y "development tools"
yum install -y zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel expat-devel
``` 

Now get python 2.7:

```
wget http://python.org/ftp/python/2.7.13/Python-2.7.13.tar.xz
tar xf Python-2.7.13.tar.xz
cd Python-2.7.13
sudo ./configure --prefix=/usr/local --enable-unicode=ucs4 --enable-shared LDFLAGS="-Wl,-rpath /usr/local/lib"
sudo make && make altinstall
```

Then pip is needed to install python packages:

```
wget https://bootstrap.pypa.io/get-pip.py
python2.7 get-pip.py
```

Now run python 2.7 in a sandbox:

```
pip2.7 install virtualenv
virtualenv my27project
source my27project/bin/activate
```

To deactivate the sandbox:

```
deactivate
```

To run the keitheyControl.py script install pySerial (maybe you have to do this in a sandbox?):

```
sudo pip2.7 install pySerial
```
