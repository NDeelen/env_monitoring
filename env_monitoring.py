#!/usr/local/bin/python

import time, datetime, subprocess, os
from array import array
import HumidityReadout
import imp
modl = imp.load_source('KITTempBoard', '/afs/cern.ch/user/c/cmstkph2/public/environmental_monitoring/current_monitoring/TemperatureReadout/KITTempBoard.py') 

isTest = False

def getTimestamp():
	return str(time.time()).rstrip('\n')

def getVoltage(ch):
	if isTest:
		commandV = ["echo","300"]
	else:
		commandV = ["python", "keithleyControl.py","-p","/dev/ttyUSB"+ch,"--rv"]
	pipe = subprocess.Popen(commandV, stdout=subprocess.PIPE)
	text = pipe.communicate()
	return text[0].rstrip("\n")


def getCurrent(ch):
	if isTest:
		commandV = ["echo","0.1"]
	else:
		commandV = ["python", "keithleyControl.py","-p","/dev/ttyUSB"+ch,"--rc"]
	pipe = subprocess.Popen(commandV, stdout=subprocess.PIPE)
	text = pipe.communicate()
	return text[0].rstrip("\n")

if __name__=='__main__':
#def update_data_file(timestamp, temperature, humidity):

	if os.path.isfile("currents.csv"):
		newFile = False
	else:
		newFile = True
	currentFile = open("currents.csv",'a')
	if newFile:
		#currentFile.write("timestamp_K0/D,voltage0/D,current0/D,voltage1/D,current1/D"+"\n")
		currentFile.write("timestamp_K0/D,voltage0/D,current0/D,timestamp_K1/D,voltage1/D,current1/D,timestamp_TH/D,Temperature/D,Humidity/D"+"\n")	
	#print "timestamp_K0 \t voltage0 \t current0 \t voltage1 \t current1"+"\n"	
	print "timestamp_K0 \t voltage0 \t current0 \t timestamp_K1 \t voltage1 \t current1 \t timestamp_T_H \t Temperature \t Humidity"+"\n"	
	currentFile.close()
	#h = HumidityReadout.Humidity()
	#t = modl.KITTempBoard()
	while True:
		currentFile = open("currents.csv",'a')
		ts_v_0 = getTimestamp()
		v0 = getVoltage("0")[5:18]
		i0 = getCurrent("0")
		ts_v_1 = getTimestamp()
		v1 = getVoltage("1")[5:18]
		i1 = getCurrent("1")

		ts_T_H = getTimestamp()                
		#T = t.getTemperature(1)
		#H = h.getHumidity()
		T = 0
		H = 0

		#currentFile.write(str(ts_v_0)+","+str(v0)+","+str(i0)+","+str(v1)+","+str(i1)+"\n")
		currentFile.write(str(ts_v_0)+","+str(v0)+","+str(i0)+","+str(ts_v_1)+","+str(v1)+","+str(i1)+","+str(ts_T_H)+","+str(T)+","+str(H)+"\n")
		currentFile.close()
		#print "!!" + str(ts_v_0)+"\t"+str(v0)+"\t"+str(i0)+"\t"+str(v1)+"\t"+str(i1)+"\n"
		print '(%f,%f) (%f,%f) Temp = %.3f Humidity = %.3f ' % ( (float)(v0) , (float)(i0) , (float)(v1) , (float)(i1) , (float)(T)  , (float)(H) )
		#print  str(ts_v_0)+"\t"+str(v0)+"\t"+str(i0)+"\t"+str(ts_v_1)+"\t"+str(v1)+"\t"+str(i1)+"\t"+str(ts_T_H)+"\t"+str(T)+"\t"+str(H)+"\n"
    
		if isTest:
			break
		else:
			time.sleep(1)
	else : 	
		print "FALSE! ERROR!"

